package br.com.mugi.exception;

import br.com.mugi.util.I18n;

/**
 *
 * @author jose.bonifacio
 */
public class BusinessException extends Exception {    
    
    private static final String MESSAGE = "exception.businessException.";
    
    private String messageCode;
    
    public BusinessException(String messageCode) {
        this(MESSAGE + messageCode, true);
        this.messageCode = messageCode;
    }
    
    public BusinessException(String message, boolean mugiMessage) {
        super(mugiMessage ? I18n.get(message) : message);
    }
    
    public String getMessageCode() {
        return messageCode;
    }    
    
}

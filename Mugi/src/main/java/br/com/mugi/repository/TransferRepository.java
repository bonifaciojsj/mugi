package br.com.mugi.repository;

import br.com.mugi.dto.TransferListDTO;
import br.com.mugi.entity.Transfer;
import br.com.mugi.entity.User;
import br.com.mugi.pagination.PagedList;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class TransferRepository extends RepositoryImpl<Transfer> {
    
    @Inject
    private DAO dao;    
    
    public Transfer findByIdFetchOriginAndTarget(Long id) {
        return dao.findSingleByQuery("SELECT t FROM Transfer t JOIN FETCH t.origin JOIN FETCH t.target WHERE t.id = " + id);        
    }
        
    public PagedList<TransferListDTO> getAllAsTransferListDTO(User user, String search, int page, int maxResultsPerPage) {
        return dao.findPagedList(TransferListDTO.class, page, maxResultsPerPage,
                "Transfer.findAllTransfersAsTransferListDTO",
                "Transfer.countFindAllTransfersAsTransferListDTO",
                new QueryParameter("search", "%" + search + "%"),
                new QueryParameter("user", user));
    }
    
}

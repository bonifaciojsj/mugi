package br.com.mugi.controller.form;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.controller.util.SessionController;
import br.com.mugi.entity.UserSettings;
import br.com.mugi.enumerated.Currency;
import br.com.mugi.enumerated.Idiom;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.UserRepository;
import br.com.mugi.service.UserService;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class SettingsController implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private UserService userService;
    @Inject
    private UserRepository userRepository;

    private UserSettings settings;

    public UserSettings getSettings() {
        if (settings == null) {
            settings = userRepository.findUserSettingsByUser(session.getUser());
        }
        return settings;
    }

    public void setSettings(UserSettings settings) {
        this.settings = settings;
    }

    public void save() {
        try {
            settings = userService.saveSettings(settings);
            FacesNavigationUtil.listenerOutcome("pretty:settings", true);
        } catch (BusinessException ex) {
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        } catch (Exception ex) {
            Logger.getLogger(AccountFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }
    
    public List<Idiom> getIdioms() {
        return Arrays.asList(Idiom.values());
    }
    
    public List<Currency> getCurrencies() {
        return Arrays.asList(Currency.values());
    }
    
    public Idiom getIdiom() {
        return this.getSettings().getIdiom();
    }
    
    public Currency getCurrency() {
        return this.getSettings().getCurrency();
    } 

}

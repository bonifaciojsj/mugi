function stopPropagation(event) {
    var evt = event;
    evt.preventDefault();
    evt.stopPropagation();
}
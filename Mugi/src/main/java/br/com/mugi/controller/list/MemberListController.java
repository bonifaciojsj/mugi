package br.com.mugi.controller.list;

import br.com.mugi.repository.MemberRepository;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class MemberListController extends ListControllerImpl implements Serializable {

    @Inject
    private MemberRepository memberRepository;
    
    @Override
    public void loadItems() {
        setItems(memberRepository.getAllAsMemberListDTO(getSearch(), this.getPage(), this.getMaxResultsPerPage()));        
    }


}

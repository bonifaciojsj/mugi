package br.com.mugi.service;

import br.com.mugi.entity.Source;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.SourceRepository;
import br.com.mugi.repository.DAO;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class SourceService {

    @Inject
    private DAO dao;
    @Inject
    private SourceRepository sourceRepository;

    public Source save(Source source) throws BusinessException {
        Source sourceDatabase = sourceRepository.findByUserAndName(source.getUser(), source.getName());
        if (sourceDatabase != null && !source.getId().equals(sourceDatabase.getId())) {
            throw new BusinessException("00103");
        }
        return this.dao.save(source);
    }
    
    public void delete(Source source) {
        this.dao.remove(source);
    }
}

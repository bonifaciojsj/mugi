package br.com.mugi.controller.login;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.controller.util.SessionController;
import br.com.mugi.entity.User;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.UserRepository;
import br.com.mugi.service.UserService;
import br.com.mugi.util.I18n;
import java.io.Serializable;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class LoginController implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private UserService userService;
    @Inject
    private UserRepository userRepository;

    private String email;
    private String password;
    private boolean keepLoggedIn;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isKeepLoggedIn() {
        return keepLoggedIn;
    }

    public void setKeepLoggedIn(boolean keepLoggedIn) {
        this.keepLoggedIn = keepLoggedIn;
    }

    public void login() {
        try {
            User user = userRepository.findByEmailAndPassword(email, password);
            if (user == null) {
                FacesMessagesUtil.addErrorMessage(I18n.get("exception.businessException.00004"), true);
                password = null;
                keepLoggedIn = false;
                return;
            }
            if (!user.isActivated()) {
                FacesMessagesUtil.addErrorMessage(I18n.get("exception.businessException.00005"), true);
                password = null;
                keepLoggedIn = false;
            }
            user.setKeepLoggedIn(keepLoggedIn);
            userService.save(user);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
            session.setUser(user);
            FacesNavigationUtil.listenerRedirect("/");
        } catch (BusinessException ex) {
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void logout() {
        session.setUser(null);
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        FacesNavigationUtil.listenerRedirect("../login/");
    }
    
    public void signUp() {        
        FacesNavigationUtil.listenerRedirect("/signUp/");
    }
}

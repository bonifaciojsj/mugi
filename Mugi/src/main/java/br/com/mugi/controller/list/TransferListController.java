package br.com.mugi.controller.list;

import br.com.mugi.controller.util.Session;
import br.com.mugi.repository.TransferRepository;
import br.com.mugi.util.CurrencyUtil;
import br.com.mugi.util.DateUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author BONIFACIO
 */
@Named
@SessionScoped
public class TransferListController extends ListControllerImpl implements Serializable {

    @Inject
    private TransferRepository transferRepository;

    @Override
    public void loadItems() {
        setItems(transferRepository.getAllAsTransferListDTO(Session.getUser(),
                getSearch(), this.getPage(), this.getMaxResultsPerPage()));
    }

    public String getDate(Date date, String pattern) {
        return DateUtil.getFormattedDate(date, pattern);
    }

    public String getValue(BigDecimal value, String locale) {
        return CurrencyUtil.getStringValue(value, locale);
    }
}

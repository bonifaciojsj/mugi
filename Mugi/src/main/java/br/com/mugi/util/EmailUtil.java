package br.com.mugi.util;

import br.com.mugi.entity.User;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.servlet.http.HttpServletRequest;
import org.codemonkey.simplejavamail.Email;
import org.codemonkey.simplejavamail.Mailer;
import org.codemonkey.simplejavamail.TransportStrategy;

/**
 *
 * @author BONIFACIO
 */
public class EmailUtil {

    private static final String HOST = "smtp.gmail.com";
    private static final int PORT = 587;
    private static final String FROM = System.getProperty("muginoreplymail");
    private static final String PASSWORD = System.getProperty("muginoreplypassword");

    public static void send(String[] to, String subject, String content) {
        Email email = new Email();
        email.setFromAddress("Mugi", FROM);
        email.setSubject(I18n.get(subject));
        for (String s : to) {
            email.addRecipient("", s, Message.RecipientType.TO);
        }
        email.setTextHTML(content);
        new Mailer(HOST, PORT, FROM, PASSWORD, TransportStrategy.SMTP_TLS).sendMail(email);
    }

    public static void sendMail001(User user) {
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream("META-INF/email/001.html");
            String[] to = {user.getEmail()};
            String subject = "email.001.subject";
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            URL reconstructedURL = new URL(request.getScheme(),
                    request.getServerName(),
                    request.getServerPort(),
                    request.getContextPath() + "/login/confirmation.xhtml?email=" + user.getEmail() + "&token=" + user.getActivationToken());
            String content = String.format(FileUtil.getStringFromInputStream(is), user.getFirstName(), reconstructedURL.toString());
            send(to, subject, content);
        } catch (MalformedURLException ex) {
            Logger.getLogger(EmailUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void sendMail002(User user) {
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream("META-INF/email/002.html");
            String[] to = {user.getEmail()};
            String subject = "email.002.subject";
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            URL reconstructedURL = new URL(request.getScheme(),
                    request.getServerName(),
                    request.getServerPort(),
                    request.getContextPath() + "/passwordRecovery.xhtml?email=" + user.getEmail() + "&token=" + user.getActivationToken());
            String content = String.format(FileUtil.getStringFromInputStream(is), user.getFirstName(), user.getPasswordRecoveryCode(), reconstructedURL.toString());
            send(to, subject, content);
        } catch (MalformedURLException ex) {
            Logger.getLogger(EmailUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

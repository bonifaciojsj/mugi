package br.com.mugi.dto;

import br.com.mugi.enumerated.Job;

/**
 *
 * @author jose.bonifacio
 */
public class PartyMemberListDTO {
    
    private final Long id;
    private final String memberNickname;
    private final String partyName;
    private final Job job;

    public PartyMemberListDTO(Long id, String memberNickname, String partyName, Job job) {
        this.id = id;
        this.memberNickname = memberNickname;
        this.partyName = partyName;
        this.job = job;
    }

    public Long getId() {
        return id;
    }

    public String getMemberNickname() {
        return memberNickname;
    }

    public String getPartyName() {
        return partyName;
    }

    public Job getJob() {
        return job;
    }
    
}

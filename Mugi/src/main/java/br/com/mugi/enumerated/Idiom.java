package br.com.mugi.enumerated;

/**
 *
 * @author jose.bonifacio
 */
public enum Idiom {
    
    ENGLISH_US("idiom.englishUS", "us");
    
    private final String description;
    private final String locale;

    private Idiom(String description, String locale) {
        this.description = description;
        this.locale = locale;
    }

    public String getDescription() {
        return description;
    }
    
    public String getLocale() {
        return locale;
    }
    
}

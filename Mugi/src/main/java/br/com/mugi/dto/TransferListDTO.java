package br.com.mugi.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author jose.bonifacio
 */
public class TransferListDTO {
    
    private final Long id;
    private final Long idOrigin;
    private final Long idTarget;
    private final String nameOrigin;
    private final String nameTarget;
    private final Date date;
    private final BigDecimal value;

    public TransferListDTO(Long id, Long idOrigin, Long idTarget, String nameOrigin, String nameTarget, Date date, BigDecimal value) {
        this.id = id;
        this.idOrigin = idOrigin;
        this.idTarget = idTarget;
        this.nameOrigin = nameOrigin;
        this.nameTarget = nameTarget;
        this.date = date;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public Long getIdOrigin() {
        return idOrigin;
    }

    public Long getIdTarget() {
        return idTarget;
    }

    public String getNameOrigin() {
        return nameOrigin;
    }

    public String getNameTarget() {
        return nameTarget;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getValue() {
        return value;
    }
    
}

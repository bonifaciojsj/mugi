package br.com.mugi.controller.login;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.entity.User;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.UserRepository;
import br.com.mugi.service.UserService;
import br.com.mugi.util.I18n;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class ConfirmationController implements Serializable {

    @Inject
    private UserService userService;
    @Inject
    private UserRepository userRepository;

    private String email;
    private String token;
    private User user;
    private boolean activated;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void loadConfirmation() {
        try {
            this.user = userRepository.findByEmailAndToken(email, token);
            if (user == null) {
                redirectError404();
            } else {
                activated = true;
                user.setActivated(true);
                user = userService.save(user);
            }
        } catch (BusinessException ex) {
            FacesMessagesUtil.addErrorMessage(I18n.get(ex.getMessage()), true);
        }
    }

    public void loadConfirmationOnlyEmail() {
        this.user = userRepository.findByEmail(email);
        if (user == null) {
            redirectError404();
        }
    }

    public void redirectError404() {
        FacesNavigationUtil.listenerRedirectError404();
    }

}

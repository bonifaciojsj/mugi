package br.com.mugi.entity;

import br.com.mugi.enumerated.TransactionType;
import br.com.mugi.validator.Required;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "transaction")
public class Transaction implements PersistentEntity<Long>, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    private Account account;
    @ManyToOne(fetch = FetchType.LAZY)
    private Source source;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Required
    private BigDecimal value;
    @Enumerated(EnumType.STRING)
    private TransactionType type;
    private boolean deferred;
    @ManyToOne(fetch = FetchType.LAZY)
    private Transfer transfer;

    public Transaction() {
        value = new BigDecimal(0);
        date = new Date();
        source = new Source();
    }

    public Transaction(Long id, Long idAccount, Long idSource) {
        this.id = id;
        this.account = new Account();
        this.source = new Source();
        this.account.setId(idAccount);
        this.source.setId(idSource);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        if (value == null) {
            value = new BigDecimal(0);
        }
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }     

    public boolean isDeferred() {
        return deferred;
    }

    public void setDeferred(boolean deferred) {
        this.deferred = deferred;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public String getName() {
        if (source != null && getDate() != null) {
            return this.source.getName() + " - " + getDate();
        }
        return "";
    }

    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.account);
        hash = 37 * hash + Objects.hashCode(this.source);
        hash = 37 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transaction other = (Transaction) obj;
        if (!Objects.equals(this.account, other.account)) {
            return false;
        }
        if (!Objects.equals(this.source, other.source)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

}

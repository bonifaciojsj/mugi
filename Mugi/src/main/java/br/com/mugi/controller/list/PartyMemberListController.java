package br.com.mugi.controller.list;

import br.com.mugi.entity.Party;
import br.com.mugi.repository.PartyMemberRepository;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class PartyMemberListController extends ListControllerImpl implements Serializable {

    @Inject
    private PartyMemberRepository partyMemberRepository;

    private Party party;

    @Override
    public void loadItems() {
        setItems(partyMemberRepository.getByPartyAsPartyMemberListDTO(getParty(), getSearch(), this.getPage(), this.getMaxResultsPerPage()));
    }

    public Party getParty() {
        if (party == null) {
            party = new Party();
        }
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }
    
    public void newParty() {
        party = null;
        clear();
    }
}

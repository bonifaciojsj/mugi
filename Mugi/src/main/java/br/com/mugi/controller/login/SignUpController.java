package br.com.mugi.controller.login;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.entity.User;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.service.UserService;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author BONIFACIO
 */
@Named
@ViewScoped
public class SignUpController implements Serializable {

    @Inject
    private UserService userService;

    private User user;

    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void signUp() {
        try {
            user = userService.save(getUser());
            FacesNavigationUtil.listenerRedirect("pretty:confirmationEmail");
        } catch (BusinessException ex) {
            if ("00101".equals(ex.getMessageCode())) {
                FacesMessagesUtil.addErrorMessage("contentForm:email", ex.getMessage(), true);
                return;
            }
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        } catch (Exception ex) {
            Logger.getLogger(SignUpController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }
}

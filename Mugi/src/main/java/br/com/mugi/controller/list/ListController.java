package br.com.mugi.controller.list;

import java.util.List;

/**
 *
 * @author jose.bonifacio
 */
public interface ListController<T> {
    
    public List getItems();
    public void clear();
    public void search();
    public void next();
    public void prev();
    public void first();
    public void last();
    public int getFirstPageResult();
    public int getLastPageResult();
    public int getTotalPages();
    public int getTotalRows();
    public void loadItems();
}

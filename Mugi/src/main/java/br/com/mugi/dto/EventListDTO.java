package br.com.mugi.dto;

import java.util.Date;

/**
 *
 * @author jose.bonifacio
 */
public class EventListDTO {
    
    private final Long id;
    private final String name;
    private final Date beginTime;
    private final Date endTime;
    private final boolean mandatory;

    public EventListDTO(Long id, String name, Date beginTime, Date endTime, boolean mandatory) {
        this.id = id;
        this.name = name;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.mandatory = mandatory;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public boolean isMandatory() {
        return mandatory;
    }    
    
}

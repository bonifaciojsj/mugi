package br.com.mugi.controller.util;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jose.bonifacio
 */
public class FacesNavigationUtil {

    public static final String FACES_REDIRECT = "?faces-redirect=true";
    public static final String FACES_REDIRECT_SEND_PARAMETERS = FACES_REDIRECT + "&includeViewParams=true";

    public static void listenerRedirectError404() {
        listenerRedirect("../error/404");
    }

    public static void listenerRedirect(String location, UrlParameter... params) {
        String urlParams = "";
        for (UrlParameter parameter : params) {
            if (urlParams.isEmpty()) {
                urlParams += "?";
            } else {
                urlParams += "&";
            }
            urlParams += parameter.getName() + "=" + parameter.getValue();
        }
        listenerRedirect(location, urlParams);
    }

    public static void listenerRedirect(String location) {
        listenerRedirect(location, "");
    }

    private static void listenerRedirect(String location, String urlParams) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(location + urlParams);
        } catch (IOException ex) {
            Logger.getLogger(FacesNavigationUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void listenerOutcome(String location) {
        listenerOutcome(location, false);
    }

    public static void listenerOutcome(String location, boolean successMessage) {
        if (successMessage) {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success", Boolean.TRUE);
        }
        FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, location);
    }

    public static String actionRedirect(String location) {
        return location + FACES_REDIRECT_SEND_PARAMETERS;
    }

    public static String actionRedirectForm(Long id) {
        return actionRedirect("form", new UrlParameter("id", id));
    }

    public static String actionRedirect(String location, UrlParameter... params) {
        String finalPath = location + FACES_REDIRECT_SEND_PARAMETERS;
        for (UrlParameter parameter : params) {
            finalPath += "&" + parameter.getName() + "=" + parameter.getValue();
        }
        return finalPath;
    }

}

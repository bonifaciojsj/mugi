package br.com.mugi.controller.form;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.controller.util.Session;
import br.com.mugi.entity.Source;
import br.com.mugi.enumerated.TransactionType;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.SourceRepository;
import br.com.mugi.service.SourceService;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class SourceFormController implements Serializable {

    @Inject
    private SourceService sourceService;
    @Inject
    private SourceRepository sourceRepository;

    private Source source;
    private List<TransactionType> types;

    public void loadSource(Long id) {
        this.source = sourceRepository.findByIdFetchUser(id);
    }

    public Source getSource() {
        if (source == null) {
            source = new Source();
        }
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public List<TransactionType> getTypes() {
        if (types == null) {
            types = Arrays.asList(TransactionType.values());
        }
        return types;
    }
    

    public void save() {
        try {
            source.setUser(Session.getUser());
            source = sourceService.save(source);
            FacesNavigationUtil.listenerOutcome("pretty:sourceEdit", true);
        } catch (BusinessException ex) {
            if ("00103".equals(ex.getMessageCode())) {
                FacesMessagesUtil.addErrorMessage("contentForm:source", ex.getMessage(), true);
                return;
            }
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        } catch (Exception ex) {
            Logger.getLogger(SourceFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void delete(Source source) {
        try {
            sourceService.delete(source);
            FacesNavigationUtil.listenerOutcome("pretty:sourceList");
        } catch (Exception ex) {
            Logger.getLogger(SourceFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }
}

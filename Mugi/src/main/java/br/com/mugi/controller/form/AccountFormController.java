package br.com.mugi.controller.form;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.controller.util.Session;
import br.com.mugi.entity.Account;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.AccountRepository;
import br.com.mugi.service.AccountService;
import br.com.mugi.util.DateUtil;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class AccountFormController implements Serializable {

    @Inject
    private AccountService accountService;
    @Inject
    private AccountRepository accountRepository;

    private Account account;
    private int hours;
    private int minutes;

    public void loadAccount(Long id) {
        this.account = accountRepository.findByIdFetchUser(id);
        setHoursAndMinutes();
    }

    public Account getAccount() {
        if (account == null) {
            account = new Account();
            setHoursAndMinutes();
        }
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }
    
    private void setHoursAndMinutes() {
        hours = DateUtil.getHours(getAccount().getOpeningDate());
        minutes = DateUtil.getMinutes(getAccount().getOpeningDate());
    }
    
    public void save() {
        try {
            DateUtil.setHours(getAccount().getOpeningDate(), hours);
            DateUtil.setMinutes(getAccount().getOpeningDate(), minutes);
            account.setUser(Session.getUser());
            account = accountService.save(account);
            FacesNavigationUtil.listenerOutcome("pretty:accountEdit", true);
        } catch (BusinessException ex) {
            if ("00102".equals(ex.getMessageCode())) {
                FacesMessagesUtil.addErrorMessage("contentForm:account", ex.getMessage(), true);
                return;
            }
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        } catch (Exception ex) {
            Logger.getLogger(AccountFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void delete(Account account) {
        try {
            accountService.delete(account);
            FacesNavigationUtil.listenerOutcome("pretty:accountList");
        } catch (Exception ex) {
            Logger.getLogger(AccountFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }
}

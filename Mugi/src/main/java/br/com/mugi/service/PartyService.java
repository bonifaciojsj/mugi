package br.com.mugi.service;

import br.com.mugi.entity.Party;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.DAO;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class PartyService {
    
    @Inject
    private DAO dao;
    
    public Party save(Party party) throws BusinessException {
        return dao.save(party);
    }
    
    public void delete(Party party) {
        dao.remove(party);
    }
}

package br.com.mugi.controller.form;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.entity.Member;
import br.com.mugi.entity.Party;
import br.com.mugi.entity.PartyMember;
import br.com.mugi.enumerated.Job;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.MemberRepository;
import br.com.mugi.repository.PartyMemberRepository;
import br.com.mugi.repository.PartyRepository;
import br.com.mugi.service.PartyMemberService;
import br.com.mugi.service.PartyService;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class PartyFormController implements Serializable {

    @Inject
    private PartyService partyService;
    @Inject
    private PartyMemberService partyMemberService;
    @Inject
    private PartyRepository partyRepository;
    @Inject
    private PartyMemberRepository partyMemberRepository;
    @Inject
    private MemberRepository memberRepository;

    private Party party;
    private PartyMember partyMember;
    private List<Job> jobs;
    private List<Member> availableMembers;

    public void loadParty(Long id) {
        this.party = partyRepository.find(Party.class, id);
    }

    public void loadView(Long id) {
        this.partyMember = partyMemberRepository.loadForSave(id);
        refreshAvailableMembers();
    }

    public void newPartyMember() {
        this.partyMember = new PartyMember();
        refreshAvailableMembers();
    }

    public Party getParty() {
        if (party == null) {
            party = new Party();
        }
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public PartyMember getPartyMember() {
        if (partyMember == null) {
            partyMember = new PartyMember();
        }
        return partyMember;
    }

    public void setPartyMember(PartyMember partyMember) {
        this.partyMember = partyMember;
    }

    public void save() {
        try {
            party = partyService.save(getParty());
            FacesNavigationUtil.listenerOutcome("pretty:partyEdit");
        } catch (BusinessException ex) {
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        } catch (Exception ex) {
            Logger.getLogger(MemberFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void delete(Party party) {
        try {
            partyService.delete(party);
            FacesNavigationUtil.listenerRedirect("partyList");
        } catch (Exception ex) {
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void saveMember() {
        try {
            if (getParty().isNew()) {
                party = partyService.save(getParty());
            }
            partyMember.setParty(party);
            partyMemberService.save(partyMember);
            FacesNavigationUtil.listenerOutcome("pretty:partyEdit");
        } catch (BusinessException ex) {
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        } catch (Exception ex) {
            Logger.getLogger(MemberFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void deleteMember() {
        try {
            partyMemberService.delete(partyMember);
            FacesNavigationUtil.listenerRedirect("pretty:partyList");
        } catch (Exception ex) {
            Logger.getLogger(MemberFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public List<Job> getJobs() {
        if (jobs == null) {
            jobs = Arrays.asList(Job.values());
        }
        return jobs;
    }

    public List<Member> getAvailableMembers() {
        if (availableMembers == null) {
            refreshAvailableMembers();
        }
        return availableMembers;
    }

    private void refreshAvailableMembers() {
        availableMembers = memberRepository.getAvailablesByPartyMember(getPartyMember());
    }

}

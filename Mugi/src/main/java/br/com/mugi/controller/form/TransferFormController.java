package br.com.mugi.controller.form;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.controller.util.Session;
import br.com.mugi.entity.Account;
import br.com.mugi.entity.Transfer;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.AccountRepository;
import br.com.mugi.repository.TransferRepository;
import br.com.mugi.service.TransferService;
import br.com.mugi.util.DateUtil;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class TransferFormController implements Serializable {

    @Inject
    private TransferService transferService;
    @Inject
    private TransferRepository transferRepository;
    @Inject
    private AccountRepository accountRepository;

    private Transfer transfer;
    private List<Account> accounts; 
    private int hours;
    private int minutes;

    public void loadTransfer(Long id) {
        transfer = transferRepository.findByIdFetchOriginAndTarget(id);
        setHoursAndMinutes();
    }

    public Transfer getTransfer() {
        if (transfer == null) {
            transfer = new Transfer();
            setHoursAndMinutes();
        }
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public List<Account> getAccounts() {
        if (accounts == null) {
            accounts = accountRepository.findByUser(Session.getUser());
        }
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    private void setHoursAndMinutes() {
        hours = DateUtil.getHours(getTransfer().getDate());
        minutes = DateUtil.getMinutes(getTransfer().getDate());
    }

    public void save() {
        try {
            DateUtil.setHours(getTransfer().getDate(), hours);
            DateUtil.setMinutes(getTransfer().getDate(), minutes);
            transfer = transferService.save(getTransfer());
            FacesNavigationUtil.listenerOutcome("pretty:transferEdit", true);
        } catch (BusinessException ex) {
            if (ex.getMessageCode().equals("00105")) {
                FacesMessagesUtil.addErrorMessage("contentForm:target", ex.getMessage(), true);
                return;
            }
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        } catch (Exception ex) {
            Logger.getLogger(TransactionFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void delete(Transfer transfer) {
        try {
            transferService.delete(transfer);
            FacesNavigationUtil.listenerOutcome("pretty:transferList");
        } catch (Exception ex) {
            Logger.getLogger(TransactionFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

}

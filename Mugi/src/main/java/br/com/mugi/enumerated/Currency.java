package br.com.mugi.enumerated;

/**
 *
 * @author jose.bonifacio
 */
public enum Currency {

    USD_DOLLAR("currency.dollarUSD", "us", "$", "currency.invalidMessage.usd"),
    BRL_REAL("currency.realBRL", "pt", "R$", "currency.invalidMessage.brl");

    private final String description;
    private final String locale;
    private final String symbol;
    private final String invalidMessage;

    private Currency(String description, String locale, String symbol, String invalidMessage) {
        this.description = description;
        this.locale = locale;
        this.symbol = symbol;
        this.invalidMessage = invalidMessage;
    }

    public String getDescription() {
        return this.description;
    }

    public String getLocale() {
        return locale;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public String getInvalidMessage() {
        return invalidMessage;
    }
}

package br.com.mugi.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author jose.bonifacio
 */
public class CurrencyUtil {

    public static String getStringValue(BigDecimal value, String locale) {
        if (value != null) {
            NumberFormat nf = NumberFormat.getNumberInstance(new Locale(locale));
            nf.setGroupingUsed(true);
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(2);
            return nf.format(value);
        }
        return "0";
    }
}

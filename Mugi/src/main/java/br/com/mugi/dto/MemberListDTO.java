package br.com.mugi.dto;

/**
 *
 * @author jose.bonifacio
 */
public class MemberListDTO {
    
    private final Long id;
    private final String nickname;
    private final String name;
    private final String ropdAddress;
    
    public MemberListDTO(Long id, String nickname) {
        this.id = id;
        this.nickname = nickname;
        this.name = null;
        this.ropdAddress = null;
    }
    
    public MemberListDTO(Long id, String nickname, String name, String ropdAddress) {
        this.id = id;
        this.nickname = nickname;
        this.name = name;
        this.ropdAddress = ropdAddress;
    }

    public Long getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public String getName() {
        return name;
    }

    public String getRopdAddress() {
        return ropdAddress;
    }
        
    
}

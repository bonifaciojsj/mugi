package br.com.mugi.controller.util;

import br.com.mugi.entity.User;
import javax.faces.context.FacesContext;
/**
 *
 * @author jose.bonifacio
 */
public class Session {

    public static User getUser() {
        return (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
    }
}

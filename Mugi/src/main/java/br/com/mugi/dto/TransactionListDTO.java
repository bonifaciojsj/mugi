package br.com.mugi.dto;

import br.com.mugi.entity.Source;
import br.com.mugi.enumerated.TransactionType;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author jose.bonifacio
 */
public class TransactionListDTO {
    
    private final Long id;
    private final Long idAccount;
    private final Long idSource;
    private final String nameAccount;
    private final String nameSource;
    private final Date date;
    private final BigDecimal value;
    private final boolean deferred;
    private final TransactionType type;
    private final boolean defaultValue;

    public TransactionListDTO(Long id, Long idAccount, Long idSource, String nameAccount, String nameSource, Date date, BigDecimal value, boolean deferred, TransactionType type) {
        this.id = id;
        this.idAccount = idAccount;
        this.idSource = idSource;
        this.nameAccount = nameAccount;
        this.nameSource = nameSource;
        this.date = date;
        this.value = value;
        this.deferred = deferred;
        this.type = type;
        this.defaultValue = (Source.INITIAL_VALUE_ID.equals(idSource) || Source.TRANSFER_ID.equals(idSource));
    }

    public Long getId() {
        return id;
    }

    public Long getIdAccount() {
        return idAccount;
    }

    public Long getIdSource() {
        return idSource;
    }

    public String getNameAccount() {
        return nameAccount;
    }

    public String getNameSource() {
        return nameSource;
    }

    public Date getDate() {
        return date;
    }
    
    public BigDecimal getValue() {
        return this.value;
    }

    public boolean getDeferred() {
        return deferred;
    }

    public TransactionType getType() {
        return type;
    }
    
    public boolean getIncome() {
        return TransactionType.INCOME.equals(type);
    }
    
    public boolean isDefaultValue() {
         return this.defaultValue;
    }
    
}

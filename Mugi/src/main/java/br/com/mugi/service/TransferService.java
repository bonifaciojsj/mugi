package br.com.mugi.service;

import br.com.mugi.entity.Source;
import br.com.mugi.entity.Transaction;
import br.com.mugi.entity.Transfer;
import br.com.mugi.enumerated.TransactionType;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.DAO;
import br.com.mugi.repository.SourceRepository;
import br.com.mugi.repository.TransactionRepository;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class TransferService {

    @Inject
    private DAO dao;
    @Inject
    private TransactionService transactionService;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private SourceRepository sourceRepository;

    public Transfer save(Transfer transfer) throws BusinessException {
        if (transfer.getOrigin().getId().equals(transfer.getTarget().getId())) {
            throw new BusinessException("00105");
        }
        if (transfer.isNew()) {
            transfer = dao.save(transfer);
            Source transferSource = sourceRepository.find(Source.class, Source.TRANSFER_ID);
            Transaction newTransactionOrigin = new Transaction();
            Transaction newTransactionTarget = new Transaction();
            prepareNewTransaction(transfer, newTransactionOrigin, transferSource, true);
            prepareNewTransaction(transfer, newTransactionTarget, transferSource, false);
            transactionService.save(newTransactionOrigin);
            transactionService.save(newTransactionTarget);
            return transfer;
        } else {
            List<Transaction> transactions = transactionRepository.findTransactionsByTransfer(transfer);
            for (Transaction transaction : transactions) {
                prepareUpdateTransaction(transfer, transaction, TransactionType.INCOME.equals(transaction.getType()));
                transactionService.save(transaction);
            }
        }
        return dao.save(transfer);
    }

    public void delete(Transfer transfer) {
        List<Transaction> transactions = transactionRepository.findTransactionsByTransfer(transfer);
        for (Transaction transaction : transactions) {
            transactionService.delete(transaction);
        }
        dao.remove(transfer);
    }

    private void prepareNewTransaction(Transfer transfer, Transaction transaction, Source source, boolean isIncome) {
        transaction.setSource(source);
        if (isIncome) {
            transaction.setAccount(transfer.getTarget());
            transaction.setType(TransactionType.INCOME);
        } else {
            transaction.setAccount(transfer.getOrigin());
            transaction.setType(TransactionType.OUTCOME);
        }
        transaction.setDate(transfer.getDate());
        transaction.setDeferred(true);
        transaction.setValue(transfer.getValue());
        transaction.setTransfer(transfer);
    }

    private void prepareUpdateTransaction(Transfer transfer, Transaction transaction, boolean isIncome) {
        if (isIncome) {
            transaction.setAccount(transfer.getTarget());
        } else {
            transaction.setAccount(transfer.getOrigin());
        }
        transaction.setDate(transfer.getDate());
        transaction.setValue(transfer.getValue());
    }
}

package br.com.mugi.repository;

import br.com.mugi.dto.PartyListDTO;
import br.com.mugi.entity.Party;
import br.com.mugi.pagination.PagedList;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class PartyRepository extends RepositoryImpl<Party>{

    @Inject
    private DAO dao;
    
    public PagedList<PartyListDTO> getAllAsPartyListDTO(String search, int page, int maxResultsPerPage) {
        String query =
                "  SELECT DISTINCT new br.com.mugi.dto.PartyListDTO(p.id, p.name)"
                + "  FROM " + Party.class.getSimpleName() + " p"
                + " WHERE p.name LIKE '%" + search + "%'"
                + " ORDER BY p.id";
        String queryCount =
                "  SELECT count(p.id)"
                + "  FROM " + Party.class.getSimpleName() + " p"
                + " WHERE p.name LIKE '%" + search + "%'";        
        return dao.findPagedList(PartyListDTO.class, query, queryCount, page, maxResultsPerPage);
    }
   
}

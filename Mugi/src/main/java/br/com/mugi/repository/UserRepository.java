package br.com.mugi.repository;

import br.com.mugi.entity.User;
import br.com.mugi.entity.UserSettings;
import br.com.mugi.util.PasswordUtil;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class UserRepository extends RepositoryImpl<User> {
    
    @Inject
    private DAO dao;
    
    public User findByEmail(String email) {
        return dao.findSingleByQuery("SELECT u FROM User u WHERE u.email = '" + email + "'");
    }
    
    public User findByEmailAndToken(String email, String token) {
        return dao.findSingleByQuery("SELECT u FROM User u WHERE u.email = '" + email + "' AND u.activationToken = '" + token + "'");
    }
    
    public User findByEmailAndPassword(String email, String password) {
        return dao.findSingleByQuery("SELECT u FROM User u WHERE u.email = '" + email + "' AND u.password = '" + PasswordUtil.getEncryptedPassword(password) + "'");        
    }
    
    public UserSettings findUserSettingsByUser(User user) {
        return dao.findSingleByQuery("SELECT us FROM UserSettings us WHERE us.user.id = " + user.getId());
    }
}

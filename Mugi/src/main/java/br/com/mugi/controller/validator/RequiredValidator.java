package br.com.mugi.controller.validator;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.validator.RequiredValidateUtil;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author jose.bonifacio
 */
@FacesValidator("requiredValidator")
public class RequiredValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (!RequiredValidateUtil.isValid(value)) {
            throw new ValidatorException(FacesMessagesUtil.newErrorMessage(RequiredValidateUtil.MESSAGE));
        }
    }
}

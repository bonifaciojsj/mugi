package br.com.mugi.repository;

/**
 *
 * @author jose.bonifacio
 */
public class QueryParameter {

    private final String name;
    private final Object value;

    public QueryParameter(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }

}

package br.com.mugi.controller.login;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.controller.util.UrlParameter;
import br.com.mugi.entity.User;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.UserRepository;
import br.com.mugi.service.UserService;
import br.com.mugi.util.EmailUtil;
import br.com.mugi.util.I18n;
import br.com.mugi.util.PasswordUtil;
import java.io.Serializable;
import java.util.UUID;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class PasswordRecoveryController implements Serializable {

    @Inject
    private UserService userService;
    @Inject
    private UserRepository userRepository;

    private String email;
    private String token;
    private String password;
    private String recoveryCode;
    private User user;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRecoveryCode() {
        return recoveryCode;
    }

    public void setRecoveryCode(String recoveryCode) {
        this.recoveryCode = recoveryCode;
    }

    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void saveNewPassword() {
        try {
            if (!recoveryCode.equals(user.getPasswordRecoveryCode())) {
                FacesMessagesUtil.addErrorMessage("contentForm:recoveryCode", I18n.get("exception.businessException.00008"), true);
            } else {
                user.setPasswordRecovery(false);
                user.setPasswordRecoveryCode(null);
                user.setPassword(PasswordUtil.getEncryptedPassword(password));
                userService.save(user);
                FacesMessagesUtil.addInfoMessage(I18n.get("exception.businessException.00009"), true);
                FacesNavigationUtil.listenerRedirect("login", new UrlParameter("email", user.getEmail()));
            }
        } catch (BusinessException ex) {
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void loadRecovery() {
        user = userRepository.findByEmailAndToken(email, token);
        if (user == null || !user.isPasswordRecovery()) {
            redirectError404();
        }
    }

    public void sendPasswordRecovery() {
        try {
            User userFound = userRepository.findByEmail(email);
            if (user == null) {
                FacesMessagesUtil.addErrorMessage("contentForm:email", I18n.get("exception.businessException.00007"), true);
            } else {
                email = null;
                userFound.setPasswordRecovery(true);
                userFound.setPasswordRecoveryCode(UUID.randomUUID().toString().substring(0, 4));
                userService.save(userFound);
                EmailUtil.sendMail002(userFound);
                FacesMessagesUtil.addInfoMessage(I18n.get("exception.businessException.00006"), true);
            }
        } catch (BusinessException ex) {
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void redirectError404() {
        FacesNavigationUtil.listenerRedirectError404();
    }
}

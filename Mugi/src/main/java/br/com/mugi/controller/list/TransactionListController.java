package br.com.mugi.controller.list;

import br.com.mugi.util.CurrencyUtil;
import br.com.mugi.util.DateUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class TransactionListController implements Serializable {

    @Inject
    private TransactionAllListController transactionAllListController;
    @Inject
    private TransactionIncomeListController transactionIncomeListController;
    @Inject
    private TransactionOutcomeListController transactionOutcomeListController;

    private String search = "";
    private int tab = 0;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
        transactionAllListController.setSearch(search);
        transactionIncomeListController.setSearch(search);
        transactionOutcomeListController.setSearch(search);
    }

    public int getTab() {
        return tab;
    }

    public void setTab(int tab) {
        this.tab = tab;
    }

    public void clear() {
        switch (tab) {
            case 0:
                transactionAllListController.clear();
                break;
            case 1:
                transactionIncomeListController.clear();
                break;
            case 2:
                transactionOutcomeListController.clear();
                break;
        }
    }
    
    public void search() {
        search(tab);
    }

    public void search(int tab) {
        this.tab = tab;
        switch (tab) {
            case 0:
                transactionAllListController.search();
                break;
            case 1:
                transactionIncomeListController.search();
                break;
            case 2:
                transactionOutcomeListController.search();
                break;
        }
    }

    public String getDate(Date date, String pattern) {
        return DateUtil.getFormattedDate(date, pattern);
    }

    public String getValue(BigDecimal value, String locale) {
        return CurrencyUtil.getStringValue(value, locale);
    }
}

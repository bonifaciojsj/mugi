package br.com.mugi.validator;

import br.com.mugi.util.I18n;

/**
 *
 * @author jose.bonifacio
 */
public class RequiredValidateUtil {
        
    public static final String MESSAGE_CODE = "00003";
    public static final String MESSAGE = I18n.get("exception.businessException." + MESSAGE_CODE);
    
    public static boolean isValid(Object value) {
        return value != null && !value.toString().isEmpty();
    }
}

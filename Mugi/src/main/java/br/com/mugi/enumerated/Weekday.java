package br.com.mugi.enumerated;

import br.com.mugi.util.I18n;

/**
 *
 * @author jose.bonifacio
 */
public enum Weekday {
    
    SUNDAY("weekday.sunday"),
    MONDAY("weekday.monday"),
    TUESDAY("weekday.tuesday"),
    WEDNESDAY("weekday.wesdnesday"),
    THURSDAY("weekday.thursday"),
    FRIDAY("weekday.friday"),
    SATURDAY("weekday.saturday");
    
    private final String i18nDescription;

    private Weekday(String i18nDescription) {
        this.i18nDescription = i18nDescription;
    }

    public String getI18nDescription() {
        return I18n.get(i18nDescription);
    }
    
}

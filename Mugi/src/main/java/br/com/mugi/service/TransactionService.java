package br.com.mugi.service;

import br.com.mugi.entity.Transaction;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.DAO;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class TransactionService {
    
    @Inject
    private DAO dao;
    
    public Transaction save(Transaction transaction) throws BusinessException {
        return dao.save(transaction);
    } 
    
    public void delete(Transaction transaction) {
        dao.remove(transaction);
    }
}

package br.com.mugi.service;

import br.com.mugi.entity.PartyMember;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.DAO;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class PartyMemberService {

    @Inject
    private DAO dao;

    public PartyMember save(PartyMember party) throws BusinessException {
        return dao.save(party);
    }

    public void delete(PartyMember party) {
        dao.remove(party);
    }
}

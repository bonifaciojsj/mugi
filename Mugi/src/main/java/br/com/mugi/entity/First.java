package br.com.mugi.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "first")
@NamedQueries({
	@NamedQuery(
	name = "findAllWithSecondList",
	query = "FROM First f LEFT JOIN FETCH f.secondList"
	)
})
@NamedEntityGraphs({
    @NamedEntityGraph(
        name = "firstWithSecondList",
        attributeNodes = {
            @NamedAttributeNode(value = "secondList")
        }
    )
})
public class First {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String fieldOne;
    private String fieldTwo;
    private String fieldThree;
    @OneToMany(mappedBy = "first")
    private List<Second> secondList;
    @OneToMany(mappedBy = "first")
    private List<Fifth> fifthList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFieldOne() {
        return fieldOne;
    }

    public void setFieldOne(String fieldOne) {
        this.fieldOne = fieldOne;
    }

    public String getFieldTwo() {
        return fieldTwo;
    }

    public void setFieldTwo(String fieldTwo) {
        this.fieldTwo = fieldTwo;
    }

    public String getFieldThree() {
        return fieldThree;
    }

    public void setFieldThree(String fieldThree) {
        this.fieldThree = fieldThree;
    }

    public List<Second> getSecondList() {
        return secondList;
    }

    public void setSecondList(List<Second> secondList) {
        this.secondList = secondList;
    }    

    public List<Fifth> getFifthList() {
        return fifthList;
    }

    public void setFifthList(List<Fifth> fifthList) {
        this.fifthList = fifthList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final First other = (First) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}

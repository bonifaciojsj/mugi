package br.com.mugi.repository;

import br.com.mugi.entity.PersistentEntity;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
public class RepositoryImpl<T extends PersistentEntity> implements Repository<T>{

    @Inject
    private DAO dao;
            
    @Override
    public T get(Class<T> t, Long id) {
        return dao.get(t, id);
    }

    @Override
    public T find(Class<T> t, Long id) {
        return dao.find(t, id);
    }    
        
}

package br.com.mugi.enumerated;

/**
 *
 * @author jose.bonifacio
 */
public enum Job {
    
    RG("Royal Guard"),
    AB("Arch Bishop");
    
    private final String fullName;

    private Job(String fullName) {
        this.fullName = fullName;
    }
    
    public String getFullName() {
        return this.fullName;
    }
}

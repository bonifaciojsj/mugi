package br.com.mugi.dto;

import br.com.mugi.enumerated.TransactionType;

/**
 *
 * @author BONIFACIO
 */
public class SourceListDTO {
    
    private final Long id;
    private final String name;    
    private final Long idUser;
    private final String nameUser;
    private final String type;

    public SourceListDTO(Long id, String name, Long idUser, String nameUser, TransactionType type) {
        this.id = id;
        this.name = name;
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.type = type.getDescription();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getIdUser() {
        return idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public String getType() {
        return type;
    }
    
}
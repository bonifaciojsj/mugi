package br.com.mugi.repository;

import br.com.mugi.dto.MemberListDTO;
import br.com.mugi.entity.Member;
import br.com.mugi.entity.PartyMember;
import br.com.mugi.pagination.PagedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class MemberRepository extends RepositoryImpl<Member> {

    @Inject
    private DAO dao;

    public PagedList<MemberListDTO> getAllAsMemberListDTO(String search, int page, int maxResultsPerPage) {
        return dao.findPagedList(MemberListDTO.class, page, maxResultsPerPage,
                "Member.findAllMembersAsMemberListDTO",
                "Member.countFindAllMembersAsMemberListDTO",
                new QueryParameter("search", "%" + search + "%"));
    }

    public List<Member> getAvailablesByPartyMember(PartyMember partyMember) {
        String query
                = "   SELECT new br.com.mugi.entity.Member(m.id, m.nickname, m.name, m.ropdAddress)"
                + "     FROM " + Member.class.getSimpleName() + " m"
                + "    WHERE NOT EXISTS (SELECT 1"
                + "                        FROM " + PartyMember.class.getSimpleName() + " pm"
                + "                       WHERE m.id = pm.member.id)";
        if (!partyMember.isNew()) {
            query += "       OR EXISTS (SELECT 1"
                    + "                   FROM " + PartyMember.class.getSimpleName() + " pm"
                    + "                  WHERE pm.id = " + partyMember.getId() + ""
                    + "                    AND pm.member.id = m.id)";
        }
        query += " ORDER BY m.nickname";
        return dao.findByQuery(query);
    }    
}

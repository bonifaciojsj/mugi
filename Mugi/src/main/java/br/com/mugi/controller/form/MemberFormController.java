package br.com.mugi.controller.form;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.entity.Member;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.MemberRepository;
import br.com.mugi.service.MemberService;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class MemberFormController implements Serializable {

    @Inject
    private MemberService memberService;
    @Inject
    private MemberRepository memberRepository;

    private Member member;

    public void loadMember(Long id) {
        this.member = memberRepository.find(Member.class, id);
    }

    public Member getMember() {
        if (member == null) {
            member = new Member();
        }
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public void save() {
        try {
            member = memberService.save(getMember());
            FacesNavigationUtil.listenerOutcome("pretty:memberEdit");
        } catch (BusinessException ex) {
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        } catch (Exception ex) {            
            Logger.getLogger(MemberFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void delete(Member member) {
        try {
            memberService.delete(member);
            FacesNavigationUtil.listenerRedirect("pretty:memberList");
        } catch (Exception ex) {
            Logger.getLogger(MemberFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }
}

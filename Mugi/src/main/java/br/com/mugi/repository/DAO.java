package br.com.mugi.repository;

import br.com.mugi.entity.PersistentEntity;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.pagination.PagedList;
import br.com.mugi.validator.EmailValidateUtil;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class DAO {

    @PersistenceContext
    private EntityManager em;

    public <T extends PersistentEntity> T save(T t) throws BusinessException {
        try {
            if (t.getId() == null) {
                em.persist(t);
                em.flush();
            } else {
                em.merge(t);
            }
            return t;
        } catch (ValidationException ex) {
            em.getTransaction().rollback();
            String message = ((ConstraintViolation) ((ConstraintViolationException) ex).getConstraintViolations().toArray()[0]).getMessage();
            if (message.equals(EmailValidateUtil.MESSAGE_CODE)) {
                throw new BusinessException(message);
            }
            throw new BusinessException(message, false);
        }
    }

    public <T extends PersistentEntity> void remove(T t) {
        try {
            t = (T) em.getReference(t.getClass(), t.getId());
            em.remove(t);
            em.flush();
        } catch (Exception ex) {
        }
    }

    /*
     * Returns a new instance from the database
     */
    public <T extends PersistentEntity> T find(Class<T> t, Long id) {
        return (T) em.find(t, id);
    }

    /*
     * Returns a reference of this object from Entity Manager
     */
    public <T extends PersistentEntity> T get(Class<T> t, Long id) {
        return (T) em.getReference(t, id);
    }

    public <T> List<T> findByQuery(String query) {
        return (List<T>) em.createQuery(query).getResultList();
    }

    public <T> T findSingleByQuery(String query) {
        List<T> result = (List<T>) em.createQuery(query).getResultList();
        if (!result.isEmpty()) {
            return (T) result.get(0);
        }
        return null;
    }

    public <T> List<T> findByNamedQuery(String queryName, QueryParameter... parameters) {
        Query query = em.createNamedQuery(queryName);
        setParameters(query, parameters);
        return (List<T>) query.getResultList();
    }

    public <T> T findSingleByNamedQuery(String queryName, QueryParameter... parameters) {
        Query query = em.createNamedQuery(queryName);
        setParameters(query, parameters);
        List<T> result = query.getResultList();
        if (!result.isEmpty()) {
            return (T) result.get(0);
        }
        return null;
    }

    public <T> PagedList findPagedList(Class<T> clazz, String query, String queryCount, int page, int maxResultPerPage) {
        int totalRows = new Integer(String.valueOf(em.createQuery(queryCount).getResultList().get(0)));
        int queryPage = page - 1;
        int firstResult = ((queryPage * maxResultPerPage));
        Query pagedQuery = em.createQuery(query).setMaxResults(maxResultPerPage).setFirstResult(firstResult);
        return new PagedList<>(pagedQuery.getResultList(), totalRows, maxResultPerPage, page);
    }

    public <T> PagedList findPagedList(Class<T> clazz, int page, int maxResultPerPage, String query, String queryCount, QueryParameter... parameters) {
        int queryPage = page - 1;
        int firstResult = ((queryPage * maxResultPerPage));
        Query pagedQuery = em.createNamedQuery(query, clazz).setMaxResults(maxResultPerPage).setFirstResult(firstResult);
        Query countQuery = em.createNamedQuery(queryCount);
        setParameters(pagedQuery, countQuery, parameters);
        List<PagedList> rows = countQuery.getResultList();
        int totalRows = 0;
        if (!rows.isEmpty()) {
            totalRows = new Integer(String.valueOf(rows.get(0)));            
        }
        return new PagedList<>(pagedQuery.getResultList(), totalRows, maxResultPerPage, page);
    }

    private void setParameters(Query query, QueryParameter... parameters) {
        setParameters(query, null, parameters);
    }

    private void setParameters(Query query, Query countQuery, QueryParameter... parameters) {
        for (QueryParameter parameter : parameters) {
            query.setParameter(parameter.getName(), parameter.getValue());
            if (countQuery != null) {
                countQuery.setParameter(parameter.getName(), parameter.getValue());
            }
        }
    }

}

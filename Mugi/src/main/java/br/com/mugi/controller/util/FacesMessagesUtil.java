package br.com.mugi.controller.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author jose.bonifacio
 */
public class FacesMessagesUtil {

    public static FacesMessage newInfoMessage(String message) {
        return new FacesMessage(FacesMessage.SEVERITY_INFO, message, null);
    }
    
    public static FacesMessage newErrorMessage(String message) {
        return new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null);
    }
    
    public static void addErrorMessage(String message, boolean keepMessage) {
        FacesContext.getCurrentInstance().addMessage(null, newErrorMessage(message));
        setKeepMessage(keepMessage);
    }

    public static void addErrorMessage(String componentId, String message, boolean keepMessage) {
        FacesContext.getCurrentInstance().addMessage(componentId, newErrorMessage(message));
        setKeepMessage(keepMessage);
    }

    public static void addInfoMessage(String message, boolean keepMessage) {
        FacesContext.getCurrentInstance().addMessage(null, newInfoMessage(message));
        setKeepMessage(keepMessage);
    }

    private static void setKeepMessage(boolean keepMessage) {
        if (keepMessage) {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        }
    }
}

create table sysuser (
id bigserial not null primary key,
name varchar(255),
email varchar(255),
password varchar(255),
activated boolean,
activationToken varchar(255),
keepLoggedIn boolean,
passwordRecovery boolean,
passwordRecoveryCode varchar(255)
);

insert into sysuser values(1, 'Mugi', 'muginoreply@gmail.com', '', false, null, false, null);

create table usersettings(
id bigserial not null primary key,
user_id bigint references sysuser(id),
idiom varchar(100),
currency varchar(100)
);

create table account (
id bigserial not null primary key,
user_id bigint references sysuser(id),
name varchar(255),
openingValue numeric(15,2)
);

insert into account values(1, 1, 'defaultValues.account.name.wallet');

create table source (
id bigserial not null primary key,
user_id bigserial references sysuser(id),
name varchar(255),
type varchar(30)
);

insert into source values(1, 1, 'defaultValues.source.name.initialValue', 'BOTH');
insert into source values(2, 1, 'defaultValues.source.name.transfer', 'BOTH');
insert into source values(3, 1, 'defaultValues.source.name.salary', 'INCOME');
insert into source values(4, 1, 'defaultValues.source.name.food', 'OUTCOME');
insert into source values(5, 1, 'defaultValues.source.name.transportation', 'OUTCOME');
insert into source values(6, 1, 'defaultValues.source.name.groceries', 'OUTCOME');

create table transfer (
id bigserial not null primary key,
origin_id bigint references account(id),
target_id bigint references account(id),
date timestamp,
value numeric(15,2)
);

create table transaction (
id bigserial not null primary key,
account_id bigint references account(id),
source_id bigint references source(id),
transfer_id bigint references transfer(id),
date timestamp,
value numeric(15,2),
type varchar(30),
deferred boolean
);

create table event (
id bigserial not null primary key,
name varchar(255),
begintime TIMESTAMP,
endtime TIMESTAMP,
mandatory boolean,
requiresAttendance boolean,
repeatable boolean,
repeatSpamDays integer
);

create table party (
id bigserial not null primary key,
name varchar(1000)
);

create table member (
id bigserial not null primary key,
nickname varchar(255),
name varchar(1000),
ropdAddress varchar(255),
email varchar(255)
);

create table partymember(
id bigserial not null primary key,
member_id bigint not null references member(id),
party_id bigint not null references party(id),
job varchar(2) not null
);

create table fourth (
id bigserial not null primary key,
fieldone varchar(255),
fieldtwo varchar(255),
fieldthree varchar(255)
);

create table first (
id bigserial not null primary key,
fieldone varchar(255),
fieldtwo varchar(255),
fieldthree varchar(255)
);

create table second (
id bigserial not null primary key,
fieldone varchar(255),
fieldtwo varchar(255),
fieldthree varchar(255),
first_id bigserial references first(id)
);

create table fifth (
id bigserial not null primary key,
fieldone varchar(255),
fieldtwo varchar(255),
fieldthree varchar(255),
first_id bigserial references first(id)
);

create table third (
id bigserial not null primary key,
fieldone varchar(255),
fieldtwo varchar(255),
fieldthree varchar(255),
second_id bigint references second(id),
fourh_id bigint references fourth(id)
);
package br.com.mugi.entity;

import br.com.mugi.util.PasswordUtil;
import br.com.mugi.validator.Email;
import br.com.mugi.validator.Required;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author BONIFACIO
 */
@Entity
@Table(name = "sysuser")
public class User implements PersistentEntity<Long> {

    public static final User MUGI_USER = new User(1L);
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Email
    private String email;
    @Required
    private String password;
    private boolean activated;
    private String activationToken;
    private boolean keepLoggedIn;
    private boolean passwordRecovery;
    private String passwordRecoveryCode;

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }
    
    public User(Long id, String email) {
        this.id = id;
        this.email = email;
    }
    
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getFirstName() {
        if (name.contains(" ")) {
            return name.split(" ")[0];
        }
        return this.name;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
    
    public void setActivationToken(String activationToken) {
        this.activationToken = activationToken;
    }

    public String getActivationToken() {
        return activationToken;
    }

    public boolean isKeepLoggedIn() {
        return keepLoggedIn;
    }

    public void setKeepLoggedIn(boolean keepLoggedIn) {
        this.keepLoggedIn = keepLoggedIn;
    }

    public boolean isPasswordRecovery() {
        return passwordRecovery;
    }

    public void setPasswordRecovery(boolean passwordRecovery) {
        this.passwordRecovery = passwordRecovery;
    }

    public String getPasswordRecoveryCode() {
        return passwordRecoveryCode;
    }

    public void setPasswordRecoveryCode(String passwordRecoveryCode) {
        this.passwordRecoveryCode = passwordRecoveryCode;
    }
    
    @Override
    public boolean isNew() {
        return this.id == null;
    }
    
    @PrePersist
    public void prePersist() {
        password = PasswordUtil.getEncryptedPassword(password);
        passwordRecovery = false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }

}

package br.com.mugi.controller.form;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.entity.Event;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.EventRepository;
import br.com.mugi.service.EventService;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class EventFormController implements Serializable {

    @Inject
    private EventService eventService;
    @Inject
    private EventRepository eventRepository;

    private Event event;

    public void loadEvent(Long id) {
        this.event = eventRepository.find(Event.class, id);
    }

    public Event getEvent() {
        if (event == null) {
            event = new Event();
        }
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public void save() {
        try {
            event = eventService.save(getEvent());
            FacesNavigationUtil.listenerOutcome("pretty:eventEdit");
        } catch (BusinessException ex) {
            if (ex.getMessageCode().equals("00100")) {
                FacesMessagesUtil.addErrorMessage("contentForm:eventTimeGrid", ex.getMessage(), true);
            }
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);           
        } catch (Exception ex) {
            Logger.getLogger(MemberFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void delete(Event event) {
        try {
            eventService.delete(event);
            FacesNavigationUtil.listenerRedirect("pretty:eventList");
        } catch (Exception ex) {
            Logger.getLogger(MemberFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }
}

package br.com.mugi.controller;

import br.com.mugi.dto.FirstDTO;
import br.com.mugi.entity.First;
import br.com.mugi.entity.Member;
import br.com.mugi.entity.Second;
import br.com.mugi.repository.FirstRepository;
import br.com.mugi.repository.MemberRepository;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class IndexController implements Serializable {

    @Inject
    private FirstRepository firstRepository;
    private List<First> firstList;
    private List<Second> secondList;
    private List<FirstDTO> firstDTOList;

    public void setFirsrList(List<First> firstList) {
        this.firstList = firstList;
    }

    public List<First> getFirstList() {
        if (firstList == null) {
            firstList = this.firstRepository.findAll();
        }
        return firstList;
    }

    public List<Second> getSecondListFromFirst() {
        if (secondList == null) {
            secondList = new ArrayList<>();
            for (First first : this.firstRepository.findAll()) {
                secondList.addAll(first.getSecondList());
            }
        }
        return secondList;
    }
    
    public List<FirstDTO> getFirstDTOList() {
        if (firstDTOList == null) {
            firstDTOList = firstRepository.findAllDTO();
        }
        return firstDTOList;
    }
    
}

package br.com.mugi.pagination;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jose.bonifacio
 */
public class PagedList<E> extends ArrayList {
    
    private int totalRows;
    private int rowsPerPage;
    private int currentPage;

    public PagedList(List<E> items, int totalRows, int rowsPerPage, int currentPage) {
        this.addAll(items);
        this.rowsPerPage = rowsPerPage;
        this.totalRows = totalRows;
        this.currentPage = currentPage;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
    
}

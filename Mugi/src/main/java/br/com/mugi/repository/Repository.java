package br.com.mugi.repository;

import br.com.mugi.entity.PersistentEntity;


/**
 *
 * @author jose.bonifacio
 * @param <T>
 */
public interface Repository<T extends PersistentEntity> {

    public T find(Class<T> t, Long id);
    public T get(Class<T> t, Long id);
    
}

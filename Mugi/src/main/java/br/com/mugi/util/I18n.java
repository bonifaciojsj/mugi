package br.com.mugi.util;

import java.util.ResourceBundle;
import javax.faces.context.FacesContext;

/**
 *
 * @author jose.bonifacio
 */
public class I18n {

    public static String get(String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle i18n = context.getApplication().evaluateExpressionGet(context, "#{i18n}", ResourceBundle.class);
        return i18n.getString(key);
    }
}

package br.com.mugi.controller.list;

import br.com.mugi.pagination.PagedList;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author jose.bonifacio
 */
public class ListControllerImpl implements ListController {

    private PagedList items;
    private int page = 1;
    private final int maxResultsPerPage = 10;
    private String search = "";

    @Override
    public PagedList getItems() {
        if (items == null) {
            loadItems();
        }
        return items;
    }

    public <T> void setItems(PagedList<T> items) {
        this.items = items;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getMaxResultsPerPage() {
        return maxResultsPerPage;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @Override
    public void next() {
        this.setPage(this.getPage() + 1);
        loadItems();
    }

    @Override
    public void prev() {
        this.setPage(this.getPage() - 1);
        loadItems();
    }

    @Override
    public void first() {
        this.setPage(1);
        loadItems();
    }

    @Override
    public void last() {
        this.setPage(getTotalPages());
        loadItems();
    }

    @Override
    public void search() {
        this.setPage(1);
        loadItems();
    }

    @Override
    public void loadItems() {
    }

    @Override
    public int getFirstPageResult() {
        return (maxResultsPerPage * (page - 1)) + 1;
    }

    @Override
    public int getLastPageResult() {
        return getItems().size() + maxResultsPerPage * (page - 1);
    }

    @Override
    public int getTotalPages() {
        return new BigDecimal(getItems().getTotalRows()).divide(new BigDecimal(maxResultsPerPage), RoundingMode.UP).intValue();
    }

    @Override
    public int getTotalRows() {
        return getItems().getTotalRows();
    }

    @Override
    public void clear() {
        setItems(null);
    }
}

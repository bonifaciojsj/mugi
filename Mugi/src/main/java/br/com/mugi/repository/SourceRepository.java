package br.com.mugi.repository;

import br.com.mugi.dto.SourceListDTO;
import br.com.mugi.entity.Source;
import br.com.mugi.entity.User;
import br.com.mugi.enumerated.TransactionType;
import br.com.mugi.pagination.PagedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class SourceRepository extends RepositoryImpl<Source> {

    @Inject
    private DAO dao;

    public Source findByIdFetchUser(Long id) {
        return dao.findSingleByQuery("SELECT s FROM Source s JOIN FETCH s.user WHERE s.id = " + id);        
    }
    
    public Source findByUserAndName(User user, String name) {
        return dao.findSingleByQuery("SELECT s FROM Source s WHERE s.user.id = " + user.getId() + " AND s.name = '" + name + "'");
    }    
    
    public List<Source> findByUser(User user) {
        return dao.findByQuery("SELECT s FROM Source s WHERE s.user.id = " + user.getId());
    }
    
    public List<Source> findByUserAndType(User user, TransactionType type) {        
        return dao.findByQuery("SELECT s FROM Source s WHERE s.user.id = " + user.getId() + " AND (s.type = '" + type.name() + "' OR s.type = 'BOTH')");
    }
    
    
    public PagedList<SourceListDTO> getAllAsSourceListDTO(User user, String search, int page, int maxResultsPerPage) {
        return dao.findPagedList(SourceListDTO.class, page, maxResultsPerPage,
                "Source.findAllSourcesAsSourceListDTO",
                "Source.countFindAllSourcesAsSourceListDTO",
                new QueryParameter("search", "%" + search + "%"),
                new QueryParameter("user", user));
    }

}

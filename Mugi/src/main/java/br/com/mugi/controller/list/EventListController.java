package br.com.mugi.controller.list;

import br.com.mugi.repository.EventRepository;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class EventListController extends ListControllerImpl implements Serializable {
    
    @Inject
    private  EventRepository eventRepository;

    @Override
    public void loadItems() {
        setItems(eventRepository.getAllAsEventListDTO(getSearch(), this.getPage(), this.getMaxResultsPerPage()));
    }

}

package br.com.mugi.service;

import br.com.mugi.entity.Account;
import br.com.mugi.entity.Source;
import br.com.mugi.entity.Transaction;
import br.com.mugi.enumerated.TransactionType;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.AccountRepository;
import br.com.mugi.repository.DAO;
import br.com.mugi.repository.SourceRepository;
import br.com.mugi.repository.TransactionRepository;
import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class AccountService {

    @Inject
    private DAO dao;
    @Inject
    private TransactionService transactionService;
    @Inject
    private SourceRepository sourceRepository;
    @Inject
    private AccountRepository accountRepository;
    @Inject
    private TransactionRepository transactionRepository;

    public Account save(Account account) throws BusinessException {
        Account accountDatabase = accountRepository.findByUserAndName(account.getUser(), account.getName());
        if (accountDatabase != null
                && (account.isNew()
                || !account.getId().equals(accountDatabase.getId()))) {
            throw new BusinessException("00102");
        }
        if (account.isNew()) {
            if (account.getOpeningValue() == null) {
                throw new BusinessException("00104");
            } else {
                account = dao.save(account);
                Source initialValueSource = sourceRepository.find(Source.class, Source.INITIAL_VALUE_ID);
                Transaction newTransaction = new Transaction();
                newTransaction.setAccount(account);
                newTransaction.setSource(initialValueSource);
                if (account.getOpeningValue().signum() > -1) {
                    newTransaction.setType(TransactionType.INCOME);
                } else {
                    newTransaction.setType(TransactionType.OUTCOME);
                }
                newTransaction.setDate(account.getOpeningDate());
                newTransaction.setDeferred(true);
                newTransaction.setValue(account.getOpeningValue());
                transactionService.save(newTransaction);
                return account;
            }
        } else {
            Transaction transaction = transactionRepository.findOpeningAccountTransactionByAccount(account);
            transaction.setDate(account.getOpeningDate());
            transaction.setValue(account.getOpeningValue());
            transactionService.save(transaction);
        }
        return this.dao.save(account);
    }

    public void delete(Account account) {
        this.dao.remove(account);
    }
}

package br.com.mugi.controller.list;

import br.com.mugi.controller.util.Session;
import br.com.mugi.enumerated.TransactionType;
import br.com.mugi.repository.TransactionRepository;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class TransactionOutcomeListController extends ListControllerImpl implements Serializable {

    @Inject
    private TransactionRepository transactionRepository;

    @Override
    public void loadItems() {
        setItems(transactionRepository.getAllAsTransactionListDTO(Session.getUser(), TransactionType.OUTCOME,
                getSearch(), this.getPage(), this.getMaxResultsPerPage()));
    }
}

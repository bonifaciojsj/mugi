package br.com.mugi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author jose.bonifacio
 */
public class DateUtil {

    public static String getFormattedDate(Date date, String pattern) {
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }
    
    public static void setHours(Date date, int hours) {
        setToDate(date, hours, Calendar.HOUR_OF_DAY);
    }
    
    public static void setMinutes(Date date, int minutes) {
        setToDate(date, minutes, Calendar.MINUTE);
    }
    
    public static int getHours(Date date) {
        return getFromDate(date, Calendar.HOUR_OF_DAY);
    }

    public static int getMinutes(Date date) {
        return getFromDate(date, Calendar.MINUTE);
    }

    private static int getFromDate(Date date, int returnType) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(returnType);
    }
    
    private static void setToDate(Date date, int value, int setType) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(setType, value);
        date.setTime(calendar.getTimeInMillis());
    }

}

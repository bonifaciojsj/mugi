package br.com.mugi.enumerated;

/**
 *
 * @author jose.bonifacio
 */
public enum TransactionType {

    INCOME("sourceType.income"),
    OUTCOME("sourceType.outcome"),
    BOTH("sourceType.both");

    private final String description;

    private TransactionType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}

package br.com.mugi.dto;

import java.math.BigDecimal;

/**
 *
 * @author BONIFACIO
 */
public class AccountListDTO {

    private final Long id;
    private final String name;
    private final Long idUser;
    private final String nameUser;
    private final BigDecimal balance;

    public AccountListDTO(Long id, String name, Long idUser, String nameUser, BigDecimal balance) {
        this.id = id;
        this.name = name;
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getIdUser() {
        return idUser;
    }

    public String getNameUser() {
        return nameUser;
    }
    
    public boolean getPositive() {
        if (balance == null) {
            return true;
        }
        return balance.signum() > -1 ;
    }

    public BigDecimal getBalance() {
        if (!getPositive()) {
            return balance.multiply(new BigDecimal(-1));
        }
        return balance;
    }

}

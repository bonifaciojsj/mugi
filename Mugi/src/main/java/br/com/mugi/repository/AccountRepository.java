package br.com.mugi.repository;

import br.com.mugi.dto.AccountListDTO;
import br.com.mugi.entity.Account;
import br.com.mugi.entity.User;
import br.com.mugi.pagination.PagedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class AccountRepository extends RepositoryImpl<Account> {

    @Inject
    private DAO dao;

    public Account findByIdFetchUser(Long id) {
        return dao.findSingleByQuery("SELECT a FROM Account a JOIN FETCH a.user WHERE a.id = " + id);        
    }
    
    public Account findByUserAndName(User user, String name) {
        return dao.findSingleByQuery("SELECT a FROM Account a WHERE a.user.id = " + user.getId() + " AND a.name = '" + name + "'");
    }
    
    public List<Account> findByUser(User user) {
        return dao.findByQuery("SELECT a FROM Account a WHERE a.user.id = " + user.getId());
    }
    
    public PagedList<AccountListDTO> getAllAsAccountListDTO(User user, String search, int page, int maxResultsPerPage) {
        return dao.findPagedList(AccountListDTO.class, page, maxResultsPerPage,
                "Account.findAllAccountsAsAccountListDTO",
                "Account.countFindAllAccountsAsAccountListDTO",
                new QueryParameter("search", "%" + search + "%"),
                new QueryParameter("user", user));
    }

}

package br.com.mugi.repository;

import br.com.mugi.dto.EventListDTO;
import br.com.mugi.entity.Event;
import br.com.mugi.pagination.PagedList;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class EventRepository extends RepositoryImpl<Event> {

    @Inject
    private DAO dao;

    public PagedList<EventListDTO> getAllAsEventListDTO(String search, int page, int maxResultsPerPage) {
        return dao.findPagedList(EventListDTO.class, page, maxResultsPerPage,
                "Event.findAllEventsAsEventListDTO",
                "Event.countFindAllEventsAsEventListDTO",
                new QueryParameter("search", "%" + search + "%"));
    }

}

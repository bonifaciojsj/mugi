package br.com.mugi.controller.form;

import br.com.mugi.controller.util.FacesMessagesUtil;
import br.com.mugi.controller.util.FacesNavigationUtil;
import br.com.mugi.controller.util.Session;
import br.com.mugi.entity.Account;
import br.com.mugi.entity.Source;
import br.com.mugi.entity.Transaction;
import br.com.mugi.enumerated.TransactionType;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.AccountRepository;
import br.com.mugi.repository.SourceRepository;
import br.com.mugi.repository.TransactionRepository;
import br.com.mugi.service.TransactionService;
import br.com.mugi.util.DateUtil;
import br.com.mugi.util.I18n;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class TransactionFormController implements Serializable {

    @Inject
    private TransactionService transactionService;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private AccountRepository accountRepository;
    @Inject
    private SourceRepository sourceRepository;

    private Transaction transaction;
    private List<Account> accounts;
    private List<Source> sources;
    private List<TransactionType> types;
    private int hours;
    private int minutes;

    public void loadTransaction(Long id) {
        this.transaction = transactionRepository.findByIdFetchTransactionAndSource(id);
        setHoursAndMinutes();
        if (getTransaction().getSource().isDefault()) {
            FacesMessagesUtil.addInfoMessage(I18n.get("transaction.message.initialValue"), true);
        }
    }

    public Transaction getTransaction() {
        if (transaction == null) {
            transaction = new Transaction();
            setHoursAndMinutes();
        }
        return transaction;
    }

    private void setHoursAndMinutes() {
        hours = DateUtil.getHours(transaction.getDate());
        minutes = DateUtil.getMinutes(transaction.getDate());
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public List<Account> getAccounts() {
        if (accounts == null) {
            accounts = accountRepository.findByUser(Session.getUser());
        }
        return accounts;
    }

    public List<Source> getSources() {
        if (sources == null) {
            if (getTransaction().getSource().isDefault()) {
                sources = new ArrayList<>();
                sources.add(getTransaction().getSource());
            } else {
                sources = sourceRepository.findByUser(Session.getUser());
            }
        }
        return sources;
    }

    public List<TransactionType> getTypes() {
        if (types == null) {
            types = new ArrayList<>();
            types.add(TransactionType.INCOME);
            types.add(TransactionType.OUTCOME);
        }
        return types;
    }

    public void save() {
        try {
            DateUtil.setHours(transaction.getDate(), hours);
            DateUtil.setMinutes(transaction.getDate(), minutes);
            transaction = transactionService.save(transaction);
            FacesNavigationUtil.listenerOutcome("pretty:transactionEdit", true);
        } catch (BusinessException ex) {
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        } catch (Exception ex) {
            Logger.getLogger(TransactionFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void delete(Transaction transaction) {
        try {
            transactionService.delete(transaction);
            FacesNavigationUtil.listenerOutcome("pretty:transactionList");
        } catch (Exception ex) {
            Logger.getLogger(TransactionFormController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessagesUtil.addErrorMessage(ex.getMessage(), true);
        }
    }

    public void changeType() {
        sources = sourceRepository.findByUserAndType(Session.getUser(), getTransaction().getType());
    }

    private void newTransaction() {
        FacesNavigationUtil.listenerOutcome("pretty:transactionNew");
    }

    public void newIncome() {
        getTransaction().setType(TransactionType.INCOME);
        newTransaction();
    }

    public void newOutcome() {
        getTransaction().setType(TransactionType.OUTCOME);
        newTransaction();
    }
}

package br.com.mugi.entity;

import br.com.mugi.enumerated.TransactionType;
import br.com.mugi.validator.Required;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "source")
public class Source implements PersistentEntity<Long>, Serializable {

    public final static Long INITIAL_VALUE_ID = 1L;
    public final static Long TRANSFER_ID = 2L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Required
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
    @Required
    private String name;
    @Required
    @Enumerated(EnumType.STRING)
    private TransactionType type;

    public Source() {
    }

    public Source(Long id, Long idUser, String name) {
        this.id = id;
        this.user = new User();
        this.user.setId(idUser);
        this.name = name;
    }
    
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }
    
    public boolean isOpeningValue() {
        return INITIAL_VALUE_ID.equals(this.id);
    }    
    
    public boolean isTransfer() {
        return TRANSFER_ID.equals(this.id);
    }
    
    public boolean isDefault() {
        return isOpeningValue() || isTransfer();
    }

    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Source other = (Source) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
}

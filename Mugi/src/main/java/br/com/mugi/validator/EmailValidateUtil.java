package br.com.mugi.validator;

import br.com.mugi.util.I18n;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author jose.bonifacio
 */
public class EmailValidateUtil {

    private static final String EMAIL_PATTERN
            = "^[_A-Za-z0-9-]+(\\."
            + "[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*"
            + "(\\.[A-Za-z]{2,})$";

    public static final String MESSAGE_CODE = "00001";
    public static final String MESSAGE = I18n.get("exception.businessException." + MESSAGE_CODE);

    public static boolean isValid(Object email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        if (email != null) {
            Matcher matcher = pattern.matcher(email.toString());
            return matcher.matches();
        }
        return false;
    }

}

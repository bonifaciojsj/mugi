package br.com.mugi.service;

import br.com.mugi.entity.Member;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.DAO;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class MemberService {
    
    @Inject
    private DAO dao;
    
    public Member save(Member member) throws BusinessException {
        return dao.save(member);
    }
    
    public void delete(Member member) {
        dao.remove(member);
    }
}

package br.com.mugi.exception;

import br.com.mugi.util.I18n;

/**
 *
 * @author jose.bonifacio
 */
public class DatabaseException extends Exception {

    private static final String MESSAGE = I18n.get("exception.databaseException");
    
    public DatabaseException() {
        super(MESSAGE);
    }
    
    public DatabaseException(Throwable cause) {
        super(MESSAGE, cause);
    }
}

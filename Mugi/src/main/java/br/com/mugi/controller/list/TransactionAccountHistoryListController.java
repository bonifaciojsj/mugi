/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mugi.controller.list;

import br.com.mugi.controller.util.Session;
import br.com.mugi.entity.Account;
import br.com.mugi.repository.AccountRepository;
import br.com.mugi.repository.TransactionRepository;
import br.com.mugi.util.CurrencyUtil;
import br.com.mugi.util.DateUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class TransactionAccountHistoryListController extends ListControllerImpl implements Serializable {

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private AccountRepository accountRepository;
    private Account account;

    @Override
    public void loadItems() {
        setItems(transactionRepository.getAllAsTransactionListDTO(Session.getUser(), account, null,
                getSearch(), this.getPage(), this.getMaxResultsPerPage()));
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
    public String getDate(Date date, String pattern) {
        return DateUtil.getFormattedDate(date, pattern);
    }

    public String getValue(BigDecimal value, String locale) {
        return CurrencyUtil.getStringValue(value, locale);
    }
    
    public void loadAccount(Long id) {
        account = accountRepository.find(Account.class, id);
        search();
    }
    
    
    @Override
    public int getMaxResultsPerPage() {
        return 5;
    }
    
        @Override
    public int getFirstPageResult() {
        return (getMaxResultsPerPage() * (getPage() - 1)) + 1;
    }

    @Override
    public int getLastPageResult() {
        return getItems().size() + getMaxResultsPerPage() * (getPage() - 1);
    }

    @Override
    public int getTotalPages() {
        return new BigDecimal(getItems().getTotalRows()).divide(new BigDecimal(getMaxResultsPerPage()), RoundingMode.UP).intValue();
    }

    @Override
    public int getTotalRows() {
        return getItems().getTotalRows();
    }

}

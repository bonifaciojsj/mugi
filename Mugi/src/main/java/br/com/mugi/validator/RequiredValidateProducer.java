package br.com.mugi.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author jose.bonifacio
 */
public class RequiredValidateProducer implements ConstraintValidator<Required, Object> {

    @Override
    public void initialize(Required field) {
    }

    @Override
    public boolean isValid(Object s, ConstraintValidatorContext constraintValidatorContext) {
        return RequiredValidateUtil.isValid(s);
    }
}
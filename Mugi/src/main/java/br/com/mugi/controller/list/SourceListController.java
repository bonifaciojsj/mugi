package br.com.mugi.controller.list;

import br.com.mugi.controller.util.Session;
import br.com.mugi.repository.SourceRepository;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author BONIFACIO
 */
@Named
@SessionScoped
public class SourceListController extends ListControllerImpl implements Serializable {

    @Inject
    private SourceRepository sourceRepository;

    @Override
    public void loadItems() {
        setItems(sourceRepository.getAllAsSourceListDTO(Session.getUser(),
                getSearch(), this.getPage(), this.getMaxResultsPerPage()));
    }
}

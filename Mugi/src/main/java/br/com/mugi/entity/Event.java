package br.com.mugi.entity;

import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "event")
public class Event implements PersistentEntity<Long> {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Temporal(TemporalType.TIMESTAMP)
    private Date beginTime;
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    private boolean mandatory;
    private boolean requiresAttendance;
    private boolean repeatable;
    private Integer repeatSpamDays;

    public Event() {
        this.beginTime = new Date();
        this.endTime = new Date();
    }

    public Event(String name, Date beginTime, Date endTime) {
        this.name = name;
        this.beginTime = beginTime;
        this.endTime = endTime;
    }
    
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public boolean isRequiresAttendance() {
        return requiresAttendance;
    }

    public void setRequiresAttendance(boolean requiresAttendance) {
        this.requiresAttendance = requiresAttendance;
    }

    public boolean isRepeatable() {
        return repeatable;
    }

    public void setRepeatable(boolean repeatable) {
        this.repeatable = repeatable;
    }

    public int getRepeatSpamDays() {
        return repeatSpamDays;
    }

    public void setRepeatSpamDays(Integer repeatSpamDays) {
        this.repeatSpamDays = repeatSpamDays;
    }
    
    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + Objects.hashCode(this.beginTime);
        hash = 67 * hash + Objects.hashCode(this.endTime);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Event other = (Event) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.beginTime, other.beginTime)) {
            return false;
        }
        if (!Objects.equals(this.endTime, other.endTime)) {
            return false;
        }
        return true;
    }
    
    
}

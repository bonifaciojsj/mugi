package br.com.mugi.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.ResourceHandler;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jose.bonifacio
 */
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = req.getSession(false);
        String loginURI = req.getContextPath() + "/login/";
        List<String> loginContextURIs = new ArrayList<>();
        loginContextURIs.add("signUp");
        loginContextURIs.add("confirmation");
        loginContextURIs.add("passwordRecovery");
        loginContextURIs.add("login");
        String errorContextURI = req.getContextPath() + "/error/";

        boolean loggedIn = session != null && session.getAttribute("user") != null;
        boolean loginRequest = req.getRequestURI().equals(loginURI);
        String[] splitContexts = req.getRequestURI().split("/");
        boolean loginContextRequest = false;
        if (splitContexts.length > 1) {
            loginContextRequest = loginContextURIs.contains(splitContexts[1]);
        }
        boolean errorContextRequest = req.getRequestURI().contains(errorContextURI);
        boolean resourceRequest = req.getRequestURI().startsWith(req.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER);

        if (loggedIn && loginContextRequest) {
            res.sendRedirect("/");
        } else if (loggedIn || loginRequest || resourceRequest || (!loggedIn && (loginContextRequest || errorContextRequest))) {
            chain.doFilter(request, response);
        } else {
            res.sendRedirect(loginURI);
        }
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}

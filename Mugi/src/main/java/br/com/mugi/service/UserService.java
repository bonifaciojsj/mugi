package br.com.mugi.service;

import br.com.mugi.entity.Account;
import br.com.mugi.entity.Source;
import br.com.mugi.entity.User;
import br.com.mugi.entity.UserSettings;
import br.com.mugi.enumerated.Currency;
import br.com.mugi.enumerated.Idiom;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.AccountRepository;
import br.com.mugi.repository.DAO;
import br.com.mugi.repository.SourceRepository;
import br.com.mugi.repository.UserRepository;
import br.com.mugi.util.EmailUtil;
import br.com.mugi.util.I18n;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author BONIFACIO
 */
@Stateless
public class UserService {

    @Inject
    private DAO dao;
    @Inject
    private UserRepository userRepository;
    @Inject
    private AccountService accountService;
    @Inject
    private SourceService sourceService;
    @Inject
    private AccountRepository accountRepository;
    @Inject
    private SourceRepository sourceRepository;

    public User save(User user) throws BusinessException {
        boolean isNew = user.isNew();
        if (isNew) {
            if (userRepository.findByEmail(user.getEmail()) != null) {
                throw new BusinessException("00101");
            }
            user.setActivationToken(UUID.randomUUID().toString());
        }
        user = dao.save(user);
        if (isNew) {
            setDefaultValues(user);            
            EmailUtil.sendMail001(user);
        }
        return dao.save(user);
    }
    
    public UserSettings saveSettings(UserSettings userSettings) throws BusinessException {
        return dao.save(userSettings);
    }

    public void delete(User user) {
        dao.remove(user);
    }
    
    private void setDefaultValues(User user) throws BusinessException {
        setDefaultAccounts(user);
        setDefaultSources(user);
        setDefaultSettings(user);
    }

    private void setDefaultAccounts(User user) throws BusinessException {
        List<Account> defaultAccounts = accountRepository.findByUser(User.MUGI_USER);
        for (Account account : defaultAccounts) {
            Account newAccount = new Account();
            newAccount.setName(I18n.get(account.getName()));
            newAccount.setUser(user);
            accountService.save(newAccount);
        }
    }

    private void setDefaultSources(User user) throws BusinessException {
        List<Source> defaultSources = sourceRepository.findByUser(User.MUGI_USER);
        for (Source source : defaultSources) {
            Source newSource = new Source();
            newSource.setUser(user);
            newSource.setName(I18n.get(source.getName()));
            newSource.setType(source.getType());
            sourceService.save(newSource);
        }
    }
    
    private void setDefaultSettings(User user) throws BusinessException {
        UserSettings newSettings = new UserSettings();
        newSettings.setUser(user);
        newSettings.setIdiom(Idiom.ENGLISH_US);
        newSettings.setCurrency(Currency.USD_DOLLAR);
        saveSettings(newSettings);
    }
}

package br.com.mugi.service;

import br.com.mugi.entity.Event;
import br.com.mugi.exception.BusinessException;
import br.com.mugi.repository.DAO;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class EventService {

    @Inject
    private DAO dao;

    public Event save(Event event) throws BusinessException {
        if (event.getBeginTime().after(event.getEndTime())) {
            throw new BusinessException("00100");
        }
        return this.dao.save(event);
    }
    
    public void delete(Event event) {
        this.dao.remove(event);
    }
}

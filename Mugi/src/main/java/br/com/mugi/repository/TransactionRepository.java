package br.com.mugi.repository;

import br.com.mugi.dto.TransactionListDTO;
import br.com.mugi.entity.*;
import br.com.mugi.enumerated.TransactionType;
import br.com.mugi.pagination.PagedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class TransactionRepository extends RepositoryImpl<Transaction> {

    @Inject
    private DAO dao;
    
    public List<Transaction> findTransactionsByTransfer(Transfer transfer) {
        return dao.findByQuery("SELECT t FROM Transaction t WHERE t.transfer.id = " + transfer.getId());
    }
    
    public Transaction findOpeningAccountTransactionByAccount(Account account) {
        return dao.findSingleByQuery("SELECT t FROM Transaction t WHERE t.source.id = " + Source.INITIAL_VALUE_ID + " AND t.account.id = " + account.getId());
    }

    public Transaction findByIdFetchTransactionAndSource(Long id) {
        return dao.findSingleByQuery("SELECT t FROM Transaction t LEFT JOIN FETCH t.account LEFT JOIN FETCH t.source WHERE t.id = " + id);
    }
    
    public PagedList<TransactionListDTO> getAllAsTransactionListDTO(User user, TransactionType type, String search, int page, int maxResultsPerPage) {
        return getAllAsTransactionListDTO(user, null, type, search, page, maxResultsPerPage);
    }
    
    public PagedList<TransactionListDTO> getAllAsTransactionListDTO(User user, Account account, TransactionType type, String search, int page, int maxResultsPerPage) {
        return dao.findPagedList(TransactionListDTO.class, page, maxResultsPerPage,
                "Transaction.findAllTransactionsAsTransactionListDTO",
                "Transaction.countFindAllTransactionsAsTransactionListDTO",
                new QueryParameter("search", "%" + search + "%"),
                new QueryParameter("user", user),
                new QueryParameter("type", type == null ? null : type),
                new QueryParameter("account", account == null ? null : account));
    }
}

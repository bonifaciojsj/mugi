package br.com.mugi.repository;

import br.com.mugi.dto.FirstDTO;
import br.com.mugi.entity.First;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class FirstRepository {

    @PersistenceContext
    private EntityManager em;

    public List<First> findAll() {
//        return em.createQuery("SELECT e FROM " + First.class.getSimpleName() + " e").setHint("javax.persistence.fetchgraph", em.getEntityGraph("firstWithSecondList")).getResultList();
        return em.createQuery("SELECT DISTINCT e FROM " + First.class.getSimpleName() + " e JOIN FETCH e.secondList secondList ORDER BY e.id", First.class).getResultList();
//        return em.createNamedQuery("findAllWithSecondList", First.class).getResultList();
    }

    public List<FirstDTO> findAllDTO() {
//        return em.createQuery("SELECT e FROM " + First.class.getSimpleName() + " e").setHint("javax.persistence.fetchgraph", em.getEntityGraph("firstWithSecondList")).getResultList();
        return em.createQuery("SELECT DISTINCT new br.com.mugi.dto.FirstDTO(e.id, secondList.id, fifthList.id) FROM " + First.class.getSimpleName() + " e JOIN e.secondList secondList JOIN e.fifthList fifthList ORDER BY e.id", FirstDTO.class).getResultList();
//        return em.createNamedQuery("findAllWithSecondList", First.class).getResultList();
    }
    
}

package br.com.mugi.dto;

/**
 *
 * @author jose.bonifacio
 */
public class PartyListDTO {
    
    private final Long id;
    private final String name;

    public PartyListDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
}

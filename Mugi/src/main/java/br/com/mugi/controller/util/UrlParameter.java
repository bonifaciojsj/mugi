package br.com.mugi.controller.util;

/**
 *
 * @author jose.bonifacio
 */
public class UrlParameter {
    
    private final String name;
    private final Object value;

    public UrlParameter(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }
    
    
}

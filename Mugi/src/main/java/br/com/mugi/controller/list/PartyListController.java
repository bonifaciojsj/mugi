package br.com.mugi.controller.list;

import br.com.mugi.repository.PartyRepository;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class PartyListController extends ListControllerImpl implements Serializable {

    @Inject
    private PartyRepository partyRepository;

    @Override
    public void loadItems() {
        setItems(partyRepository.getAllAsPartyListDTO(getSearch(), this.getPage(), this.getMaxResultsPerPage()));
    }
}

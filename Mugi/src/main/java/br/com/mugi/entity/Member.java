package br.com.mugi.entity;

import br.com.mugi.validator.Email;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "member")
public class Member implements PersistentEntity<Long> {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nickname;
    private String name;
    private String ropdAddress;
    @Email
    private String email;

    public Member() {
    }
    
    public Member(Long id, String nickname, String name, String ropdAddress) {
        this.id = id;
        this.nickname = nickname;
        this.name = name;
        this.ropdAddress = ropdAddress;
    }
    
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRopdAddress() {
        return ropdAddress;
    }

    public void setRopdAddress(String ropdAddress) {
        this.ropdAddress = ropdAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.nickname);
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + Objects.hashCode(this.ropdAddress);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Member other = (Member) obj;
        if (!Objects.equals(this.nickname, other.nickname)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.ropdAddress, other.ropdAddress)) {
            return false;
        }
        return true;
    }
    
    
}

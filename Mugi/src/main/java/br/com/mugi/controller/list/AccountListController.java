package br.com.mugi.controller.list;

import br.com.mugi.controller.util.Session;
import br.com.mugi.repository.AccountRepository;
import br.com.mugi.util.CurrencyUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author BONIFACIO
 */
@Named
@SessionScoped
public class AccountListController extends ListControllerImpl implements Serializable {

    @Inject
    private AccountRepository accountRepository;

    @Override
    public void loadItems() {
        setItems(accountRepository.getAllAsAccountListDTO(Session.getUser(),
                getSearch(), this.getPage(), this.getMaxResultsPerPage()));
    }    
    
    public String getValue(BigDecimal value, String locale) {
        return CurrencyUtil.getStringValue(value, locale);
    }
}

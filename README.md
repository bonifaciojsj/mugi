Mugi is a personal financies management software built using Java (JSF).

Project stack:

- Java;
- JSF;
- Hibernate;
- WildFly;
- PostgreSQL;
- BootsFaces;

If you want to run this project please contact me at bonifacio.jsj@gmail.com and I will help you to setup the application correctly.
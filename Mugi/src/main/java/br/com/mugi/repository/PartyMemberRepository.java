package br.com.mugi.repository;

import br.com.mugi.dto.PartyMemberListDTO;
import br.com.mugi.entity.Party;
import br.com.mugi.entity.PartyMember;
import br.com.mugi.pagination.PagedList;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class PartyMemberRepository extends RepositoryImpl<PartyMember>{
    
    @Inject
    private DAO dao;
    
    public PartyMember loadForSave(Long id) {        
        return dao.findSingleByNamedQuery(
                "PartyMember.loadForSave",
                new QueryParameter("id", id));
    }
   
    public PagedList<PartyMemberListDTO> getByPartyAsPartyMemberListDTO(Party party, String search, int page, int maxResultsPerPage) {
        String query =
                "  SELECT DISTINCT new br.com.mugi.dto.PartyMemberListDTO(pm.id, pm.member.nickname, pm.party.name, pm.job)"
                + "  FROM " + PartyMember.class.getSimpleName() + " pm"
                + " WHERE pm.party.id = " + party.getId()
                + "   AND (pm.member.nickname LIKE '%" + search + "%'"
                + "        OR pm.job LIKE '%" + search + "%')"
                + " ORDER BY pm.id";
        String queryCount =
                "  SELECT count(pm.id)"
                + "  FROM " + PartyMember.class.getSimpleName() + " pm"
                + " WHERE pm.party.id = " + party.getId()
                + "   AND (pm.member.nickname LIKE '%" + search + "%'"
                + "        OR pm.job LIKE '%" + search + "%')";
        return dao.findPagedList(PartyMemberListDTO.class, query, queryCount, page, maxResultsPerPage);
    }
   
}
